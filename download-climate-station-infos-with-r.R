## Philipp Gärtner, June 2013
## Please install the following R packages first:

## install.packages(c("RCurl", "XML", "reshape", "stringr", "ggplot2", "ggmap", "XLConnect", "RSQLite"))


library(RCurl)
library(XML)
library(reshape)
library(stringr)


url.stations <- c("ANDIR/518480.htm",
                  "AR-TUX/517050.htm", 
                  "ALAR/517300.htm",
                  "BACHU/517160.htm",
                  "BAYANBULAK/515420.htm",
                  "BAICHENG_BAY/516330.htm",
                  "CHUGIRTY/515260.htm",
                  "Hotan/518280.htm",
                  "BALGUNTAY/514670.htm",
                  "LUNTAI_BUGUR/516420.htm",
                  "AKQI/517110.htm",
                  "KUQA/516440.htm",
                  "Kashi/517090.htm",
                  "KORLA/516560.htm",
                  "RUOQIANG/517770.htm",
                  "SHACHE/518110.htm",
                  "MINFENG/518390.htm",
                  "PISHAN/518180.htm",
                  "WEN-SU/516280.htm")

station.name <- character(length = length(url.stations))
station      <- data.frame()



# download URL
# read the text lines from the provided URL
# parse the content of ```webpage``` and generate an R structure representing the HTML tree
# identify the HTML nodes of ```h2``` (header) and ```b``` bold text and store the content in a variable (I used [selectorgadget](http://selectorgadget.com/) in order to identify the required nodes).
# extract the required information from the string using the ```word{stringr}``` function
# combine station info's with ```rbind``` and store them in the data.frame



for(i in 1:length(url.stations))  {

  theurl   <- str_c("http://www.tutiempo.net/clima/", url.stations[i])
  webpage  <- getURL(theurl)
  webpage  <- readLines(tc <- textConnection(webpage)); close(tc)
  pagetree <- htmlTreeParse(webpage, error=function(...){}, 
  useInternalNodes = TRUE)

  h2 <- xpathSApply(pagetree, "//h2", xmlValue)
  b  <- xpathSApply(pagetree, "//b", xmlValue)

  name   <- word(h2[1], 3)
  start  <- as.integer(word(h2[1], 5))
  end    <- as.integer(word(h2[1], 7))

  lat    <- as.double(b[2])
  lon    <- as.double(b[3])
  alt    <- as.double(b[4])
  ID.raw <- as.character(b[1])
  ID     <- as.integer(str_sub(ID.raw, 1, 6))

  station.name[i] <- name
  station     <- rbind(station, c(ID, lat, lon, alt, start, end))

} #end loop



# add station names, provide column names and ```order``` the data.frame by the 'ending' info

station$name   <- station.name
names(station) <- c("ID", "latitude", "longitude", 
                    "altitude", "starting", "ending", "name") 
station        <- station[ order(station$ending),]

station$duration <- station$ending - station$starting

# create a map with the station location over Google map with the recording duration set to ```size = duration```.

library(ggplot2)
library(ggmap)

bbox     <- make_bbox(longitude, latitude, data = station, f= .10)
xinjiang <- get_map(location=bbox, zoom = 6, maptype = 'terrain')

map.points.xinj <- ggmap(xinjiang) + geom_point(aes(x = longitude, y = latitude, 
                                                    size = duration), data = station, 
                                                alpha = .8, colour = "black") 

map.points.xinj

################################################
#RSQLite section
################################################


library(XLConnect)
library(RSQLite)

#open a connection to the tutiempo.sqlite database
db <- dbConnect(SQLite(), dbname="tutiempo.sqlite")

#create table from data.frame
dbWriteTable(conn = db, name = "station", value = station,
             row.names = TRUE, header = TRUE)

dbListTables(db)                   # The tables in the database
dbListFields(db, "station")        # The columns in a table

dbDisconnect(db)                   # Close connection
